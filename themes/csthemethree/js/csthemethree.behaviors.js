(function ($) {

  Drupal.behaviors.csthemethreeBehavior = {
    attach: function (context, settings) {

        $('.main-nav li.level-1').hoverIntent({    
          over: function() { 
            $(this).addClass("hover");
            $('ul:first', this).show(); 
          },
          out: function() {
            $(this).removeClass("hover");
            $('ul:first', this).hide(); 
          },
          timeout: 200, 
          sensitivity: 3, 
          interval: 100
        }); 
    }
  };

})(jQuery);
