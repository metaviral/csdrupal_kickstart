<?php

/**
 * @file
 * Contains a pre-process hook for 'panels-pane'.
 */

/**
 * Implements hook_preprocess_panelizer_view_mode().
 */
function csbasetheme_preprocess_panelizer_view_mode(&$variables) { #dpm($variables); dpm($variables['element']['#panelizer']);
	$element = $variables['element'];
	$entity_type = isset($element['#entity_type']) ? $element['#entity_type'] : NULL;
	$replace_classes_array = array();

	if (isset($element['#node']) && $node = $element['#node']) {

	  $css_node_type = drupal_clean_css_identifier($node->type);
  	$css_view_mode = drupal_clean_css_identifier($element['#view_mode']);

	  // Add article ARIA role.
	  $variables['attributes_array']['role'] = 'article';

	  // Add BEM element classes.
	  $variables['title_attributes_array']['class'][] = 'node__title';
	  $variables['content_attributes_array']['class'][] = 'node__content';

	  if (isset($variables['links'])) {
	 		$variables['links']['#attributes']['class'][] = 'node__links';
	 	}

	  // Add modifier classes for view mode.
	  $variables['classes_array'][] = 'node--' . $css_view_mode;
	  $variables['classes_array'][] = 'node--' . $css_node_type . '--' . $css_view_mode;

	  // Remove classes
		$variables['classes_array'] = array_diff($variables['classes_array'], array(
			'panelizer-view-mode',
			'node-' . $css_view_mode,
		));

		// Add class replacement patterns
	  $replace_classes_array['/^node-promoted$/'] = 'node--promoted';
	  $replace_classes_array['/^node-sticky$/'] = 'node--sticky';
	}

	$replace_classes_array['/^' . preg_quote($entity_type . '-' . $element['#view_mode']) . '$/'] = $entity_type . '--' . $element['#view_mode'];
	$replace_classes_array['/^' . preg_quote($entity_type . '-' . $element['#panelizer_bundle']) . '$/'] =  $entity_type . '--' . $element['#panelizer_bundle'];
	$replace_classes_array['/^' . preg_quote($entity_type . '-' . $element['#panelizer_entity_id']) . '$/'] = $entity_type . '--' . $element['#panelizer_entity_id'];

	// Replace class arrays
	$variables['classes_array'] = preg_replace(array_keys($replace_classes_array), array_values($replace_classes_array), $variables['classes_array']);
}
