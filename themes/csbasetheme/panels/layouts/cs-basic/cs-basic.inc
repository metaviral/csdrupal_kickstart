<?php

/**
 * @file
 * Defines a simple x-columns grid layout.
 */

// Plugin definition.
$plugin = array(
  'title' => t('CS basic'),
  'icon' => 'preview.png',
  'category' => t('Omega content layouts'),
  'theme' => 'cs_basic',
  //'css' => '../../../css/layouts/cs-basic/cs-basic.layout.css',
  'regions' => array(
    'top' => t('Top'),
    'first' => t('First'),
    'second' => t('Second'),
    'third' => t('Third'),
    'fourth' => t('Fourth'),
    'fifth' => t('Fifth'),
    'sixth' => t('Sixth'),
    'seventh' => t('Seventh'),
    'eighth' => t('Eighth'),
    'ninth' => t('Ninth'),
    'bottom' => t('Bottom'),
  ),
);

/**
 * Implements hook_preprocess_cs_basic().
 */
function template_preprocess_cs_basic(&$variables) {
  $variables['attributes_array']['class'][] = 'panel-cs-basic';
  $variables['attributes_array']['class'][] = 'panel-display--cs-basic';

  foreach($variables['content'] as $name => $item) {
    $variables['region_attributes_array'][$name]['class'][] = 'cs-basic-region';
    $variables['region_attributes_array'][$name]['class'][] = 'cs-basic-region__' . drupal_clean_css_identifier($name);
  }
}
