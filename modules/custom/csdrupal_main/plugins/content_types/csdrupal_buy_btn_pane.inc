<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('CS: Buy btn'),
  // 'icon' => 'icon_node.png',
  // 'description' => t('The date the referenced node was created.'),
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'category' => t('Node'),
  'render callback' => 'csdrupal_buy_btn_pane_content_type_render'
);

function csdrupal_buy_btn_pane_content_type_render($subtype, $conf, $panel_args, $context) { #dpm($subtype); dpm($conf); dpm($context);
  
  if (empty($context) || empty($context->data)) {
    return;
  }
  
  // Get a shortcut to the node.
  $node = $context->data; #dpm($node);

  // Build the content type block.
  $block = new stdClass();
  // $block->module  = 'node_created';
  // $block->title   = t('Created date');
  // $block->delta   = $node->nid;

  $block->content = '';

  if ($trg_product = csdrupal_tracker_get_buy_url($node)) {
    $block->content = '<a href="' . $trg_product . '" class="btn btn--buy">' . variable_get('csdrupal_buy_btn_text', 'Buy now!') . '</a>';
  }

  return $block;
}

/**
 * Render the custom content type.
 */
// function ctools_node_created_content_type_render($subtype, $conf, $panel_args, $context) {
//   if (empty($context) || empty($context->data)) {
//     return;
//   }

//   // Get a shortcut to the node.
//   $node = $context->data;

//   // Build the content type block.
//   $block = new stdClass();
//   $block->module  = 'node_created';
//   $block->title   = t('Created date');
//   $block->content = format_date($node->created, $conf['format']);
//   $block->delta   = $node->nid;

//   return $block;
// }

